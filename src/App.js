import './App.css';
import { useEffect, useState } from 'react';
import axios from 'axios';

function App() {
  const [upcomingWeather, setUpcomingWeather] = useState([]);
  const { REACT_APP_ENDPOINT_URL } = process.env;

  useEffect(() => {
    axios
      .get(REACT_APP_ENDPOINT_URL)
      .then((data) => setUpcomingWeather(JSON.parse(data.data.message)));
  }, [REACT_APP_ENDPOINT_URL]);

  return (
    <div className='App'>
      <header className='App-header'>
        <p>Weather for the next seven days:</p>
        <ul>
          {upcomingWeather.map((day, idx) => {
            day = day[0];
            return (
              <li key={idx} style={{ textAlign: 'left' }}>
                {day.main}: {day.description} |{' '}
                <b>{day.main === 'Rain' ? 'Car' : 'Bike'}</b>
              </li>
            );
          })}
        </ul>
      </header>
    </div>
  );
}

export default App;
