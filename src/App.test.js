import { render, screen } from '@testing-library/react';
import App from './App';

test('renders advice for commuters', () => {
  render(<App />);
  const commuteAdvice = screen.getByText(/Weather for the next seven days/i);
  expect(commuteAdvice).toBeInTheDocument();
});
