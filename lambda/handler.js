const axios = require('axios');

module.exports.getWeather = async (event) => {
  const suggestedCommuteResult = await suggestedCommute();
  const statusCode =
    suggestedCommuteResult === 'Something went wrong' ? 500 : 200;
  return {
    statusCode: statusCode,
    headers: {
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Origin':
        'http://commute-website.s3-website-us-east-1.amazonaws.com',
      'Access-Control-Allow-Methods': 'OPTIONS,POST,GET',
    },
    body: JSON.stringify(
      {
        message: JSON.stringify(suggestedCommuteResult, null, 2),
      },
      null,
      2
    ),
  };
};

async function suggestedCommute() {
  return new Promise(function (resolve, reject) {
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/onecall?lat=-41.2866&lon=174.7756&exclude=minutely,hourly&appid=${process.env.APP_ID}`
      )
      .then(function (response) {
        const dailyWeather = response.data.daily.map((day) => {
          return day.weather;
        });
        resolve(dailyWeather);
      })
      .catch(function (error) {
        console.error(error);
        return reject('Something went wrong');
      });
  });
}
