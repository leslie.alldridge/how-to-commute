data "template_file" "bucket_policy" {
  template = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid       = "PublicReadGetObject"
        Effect    = "Allow"
        Principal = "*"
        Action    = "s3:GetObject"
        Resource = [
          "${aws_s3_bucket.website.arn}/*",
        ]
      },
    ]
  })
  vars = {
    bucket = aws_s3_bucket.website.arn
  }
}

resource "aws_s3_bucket" "website" {
  bucket = "commute-website"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}

resource "aws_s3_bucket_policy" "public_read" {
  bucket = aws_s3_bucket.website.id
  policy = data.template_file.bucket_policy.rendered
}
